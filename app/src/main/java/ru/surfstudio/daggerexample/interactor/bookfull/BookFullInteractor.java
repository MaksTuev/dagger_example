package ru.surfstudio.daggerexample.interactor.bookfull;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.app.PerApplication;
import ru.surfstudio.daggerexample.module.player.PlayerBookStateProvider;
import ru.surfstudio.daggerexample.module.repository.BookRepository;

/**
 * класс, поставляющий {@link BookFull}
 * илпользуется только для UI
 */
@PerApplication
public class BookFullInteractor {

    private BookRepository bookRepository;
    private PlayerBookStateProvider playerBookStateProvider;

    @Inject
    public BookFullInteractor(BookRepository bookRepository, PlayerBookStateProvider playerBookStateProvider) {
        this.bookRepository = bookRepository;
        this.playerBookStateProvider = playerBookStateProvider;
    }

    //todo
}
