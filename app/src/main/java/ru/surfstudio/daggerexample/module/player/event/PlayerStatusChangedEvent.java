package ru.surfstudio.daggerexample.module.player.event;

import ru.surfstudio.daggerexample.entity.Book;
import ru.surfstudio.daggerexample.entity.bookstate.PlayerBookState;

/**
 * событие изменения статуса плеера
 */
public class PlayerStatusChangedEvent {

    private Book book;

    private PlayerBookState playerBookState;

    public Book getBook() {
        return book;
    }

    public PlayerBookState getPlayerBookState() {
        return playerBookState;
    }
}
