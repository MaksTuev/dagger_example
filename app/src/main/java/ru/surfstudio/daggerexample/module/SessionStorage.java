package ru.surfstudio.daggerexample.module;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.app.PerApplication;
import ru.surfstudio.daggerexample.util.SettingsUtil;

/**
 * хранилище для текущей сессии пользователя
 */
@PerApplication
public class SessionStorage {
    public static final String PREFERENCES_SESSION = "preferences_session";
    private static final String KEY_SESSION = "KEY_SESSION";
    private final SharedPreferences mSessionSettings;
    private Context mAppContext;

    @Inject
    public SessionStorage(Context appContext) {
        this.mAppContext = appContext;
        mSessionSettings = getSessionSharedPreferences(this.mAppContext);
    }

    public String getSession() {
        return SettingsUtil.getString(mAppContext, mSessionSettings, KEY_SESSION);
    }

    public void setSession(String session) {
        SettingsUtil.putString(mAppContext, mSessionSettings, KEY_SESSION, session);
    }

    public void clearSession() {
        SettingsUtil.putString(mAppContext, mSessionSettings, KEY_SESSION, SettingsUtil.EMPTY_STRING_SETTING);
    }

    public boolean isSessionEmpty() {
        return getSession().equals(SettingsUtil.EMPTY_STRING_SETTING);
    }

    private SharedPreferences getSessionSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES_SESSION, Context.MODE_PRIVATE);
    }
}