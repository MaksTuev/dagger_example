package ru.surfstudio.daggerexample.module.player;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.app.PerApplication;
import ru.surfstudio.daggerexample.entity.bookstate.PlayerBookState;
import ru.surfstudio.daggerexample.module.player.core.AudioPlayerCore;
import ru.surfstudio.daggerexample.module.player.event.PlayerStatusChangedEvent;
import rx.Observable;

/**
 * высокоуровневый плеер, позволяющий воспроизводить книги
 */
@PerApplication
public class BookAudioPlayer {

    private AudioPlayerCore audioPlayerCore;

    @Inject
    public BookAudioPlayer(AudioPlayerCore audioPlayerCore) {
        this.audioPlayerCore = audioPlayerCore;
    }

    public Observable<PlayerBookState> observeAudioProgress(){
        return Observable.empty(); //todo
    }

    //todo error
    public Observable<PlayerStatusChangedEvent> observePlayerStatusChanged(){
        return Observable.empty(); //todo
    }
}
