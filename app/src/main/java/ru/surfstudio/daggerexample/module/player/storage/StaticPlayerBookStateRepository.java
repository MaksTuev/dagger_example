package ru.surfstudio.daggerexample.module.player.storage;


import javax.inject.Inject;

import ru.surfstudio.daggerexample.app.PerApplication;
import rx.Observable;

/**
 * хранилище для {@link StaticPlayerBookState}
 * в хранилище не хранится информация о статусе плеера
 */
@PerApplication
public class StaticPlayerBookStateRepository {
    @Inject
    public StaticPlayerBookStateRepository() {
    }

    public Observable<StaticPlayerBookState> getStaticPlayerBookState(String bookId) {
        return null;
        //todo
    }
}
