package ru.surfstudio.daggerexample.module.repository;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.app.PerApplication;
import ru.surfstudio.daggerexample.entity.Book;
import ru.surfstudio.daggerexample.module.SessionStorage;
import rx.Observable;
@PerApplication
public class BookRepositoryImpl implements BookRepository {

    private SessionStorage sessionStorage;

    @Inject
    public BookRepositoryImpl(SessionStorage sessionStorage) {
        this.sessionStorage = sessionStorage;
    }

    public Observable<Book> observeMyBookChanged(){
        return Observable.empty();
    }
}
