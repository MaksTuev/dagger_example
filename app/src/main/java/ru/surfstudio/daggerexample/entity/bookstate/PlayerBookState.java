package ru.surfstudio.daggerexample.entity.bookstate;

/**
 * состояние книги в плеере
 */
public class PlayerBookState {

    private String bookId;

    private int listenPosition;

    private int listenChapter;

    private double speedRatio;

    private PlayingStatus playingStatus;

    public PlayerBookState(String bookId, int listenChapter, int listenPosition, double speedRatio, PlayingStatus currentPlayingStatus) {
        this.bookId = bookId;
        this.listenChapter = listenChapter;
        this.listenPosition = listenPosition;
        this.speedRatio = speedRatio;
        this.playingStatus = currentPlayingStatus;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }


    public double getSpeedRatio() {
        return speedRatio;
    }

    public void setSpeedRatio(double speedRatio) {
        this.speedRatio = speedRatio;
    }

    @Override
    public String toString() {
        return "PlayerBookState{" +
                "bookId='" + bookId + '\'' +
                ", listenPosition=" + listenPosition +
                ", listenChapter=" + listenChapter +
                ", speedRatio=" + speedRatio +
                ", playingStatus=" + playingStatus +
                '}';
    }

    public boolean wasListened() {
        return listenChapter != 0
                || listenPosition != 0;
    }

    public boolean isNeverListened() {
        return listenChapter == 0
                && listenPosition == 0;
    }
}
