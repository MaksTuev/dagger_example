package ru.surfstudio.daggerexample.entity.booksource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс инкапсулирующий локальные источники данных книги для проигрывателя
 */
public class LocalBookSources implements Serializable {

    private ArrayList<FileChapterSource> fileSources;

    public LocalBookSources(ArrayList<FileChapterSource> fileChapterSources) {
        this.fileSources = fileChapterSources;
    }

    public boolean contains(int chapter) {
        return getChapterSource(chapter) != null && getChapterSource(chapter).getSource() != null;
    }

    public FileChapterSource getChapterSource(int chapter) {
        for (FileChapterSource chapterSource : fileSources) {
            if (chapterSource.getChapter() == chapter) {
                return chapterSource;
            }
        }
        return null;
    }

    public void add(FileChapterSource fileChapterSource) {
        FileChapterSource oldFileChapterSource = getChapterSource(fileChapterSource.getChapter());
        if(oldFileChapterSource!=null){
            fileSources.remove(oldFileChapterSource);
        }
        fileSources.add(fileChapterSource);
    }

    public int getNumChapters() {
        return fileSources.size();
    }

    @Override
    public String toString() {
        return "LocalBookSources{" +
                "fileSources=" + fileSources +
                '}';
    }

    public List<FileChapterSource> getChapterSources() {
        return new ArrayList<>(fileSources);
    }

    public void clear() {
        fileSources.clear();
    }

    public void delete(FileChapterSource fileChapterSource) {
        fileSources.remove(fileChapterSource);
    }
}
