package ru.surfstudio.daggerexample.entity.bookstate;

public enum PlayingStatus {
    PLAY,
    PAUSE,
    STOP,
    BUFFERING
}
