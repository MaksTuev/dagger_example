package ru.surfstudio.daggerexample.entity.booksource;

public interface ChapterSource {
    int CHAPTER_TRIAL = -1;

    String getSource();
}
