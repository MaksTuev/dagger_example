package ru.surfstudio.daggerexample.app;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.surfstudio.daggerexample.module.repository.RepositoryModule;

@Module(includes = {RepositoryModule.class})
public class AppModule {
    private Context appContext;

    public AppModule(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    @PerApplication
    public Context provideContext(){
        return appContext;
    }

}
