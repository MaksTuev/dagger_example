package ru.surfstudio.daggerexample.ui.screen.main;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.module.player.BookAudioPlayer;
import ru.surfstudio.daggerexample.module.repository.BookRepository;
import ru.surfstudio.daggerexample.ui.base.BasePresenter;
import ru.surfstudio.daggerexample.ui.base.activity.PerActivity;


@PerActivity
public class MainPresenter extends BasePresenter<MainActivityView> {

    private BookAudioPlayer bookAudioPlayer;
    private BookRepository bookRepository;

    @Inject
    public MainPresenter(BookAudioPlayer bookAudioPlayer, BookRepository bookRepository) {
        this.bookAudioPlayer = bookAudioPlayer;
        this.bookRepository = bookRepository;
    }

    @Override
    public void onLoad() {
        super.onLoad();
        //todo

    }
}
