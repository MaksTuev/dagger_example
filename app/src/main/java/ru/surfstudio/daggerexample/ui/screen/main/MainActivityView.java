package ru.surfstudio.daggerexample.ui.screen.main;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.ui.base.BasePresenter;
import ru.surfstudio.daggerexample.ui.base.activity.ActivityModule;
import ru.surfstudio.daggerexample.ui.base.activity.BaseActivityView;


public class MainActivityView extends BaseActivityView {

    @Inject
    MainPresenter presenter;

    @Override
    protected void satisfyDependencies() {
        DaggerMainComponent.builder()
                .appComponent(getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .build()
                .inject(this);
    }

    @Override
    protected int getContentView() {
        return 0;
    }

    @Override
    public String getName() {
        return "Main";
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }
}
