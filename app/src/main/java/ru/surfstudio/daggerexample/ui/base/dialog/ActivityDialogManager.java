package ru.surfstudio.daggerexample.ui.base.dialog;

import android.app.Activity;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.ui.base.fragment.PerFragment;

/**
 * отвечает за показывание и скрывание диалогов из презентера, когда вью является Activity
 */
@PerFragment
public class ActivityDialogManager implements DialogManager {
    private Activity activity;

    @Inject
    public ActivityDialogManager(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void show(BaseDialog dialog) {
        dialog.show(activity);
    }
}
