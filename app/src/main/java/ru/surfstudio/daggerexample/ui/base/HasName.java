package ru.surfstudio.daggerexample.ui.base;

public interface HasName {
    String getName();
}
