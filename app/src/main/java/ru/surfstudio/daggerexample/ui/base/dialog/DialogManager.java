package ru.surfstudio.daggerexample.ui.base.dialog;

/**
 * отвечает за показывание и скрывание диалогов из презентера
 */
public interface DialogManager {
    void show(BaseDialog dialog);

}
