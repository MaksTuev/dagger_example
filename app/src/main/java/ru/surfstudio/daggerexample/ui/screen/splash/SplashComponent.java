package ru.surfstudio.daggerexample.ui.screen.splash;

import dagger.Component;
import ru.surfstudio.daggerexample.ui.base.activity.ContainerActivityComponent;
import ru.surfstudio.daggerexample.ui.base.fragment.FragmentModule;
import ru.surfstudio.daggerexample.ui.base.fragment.PerFragment;

@PerFragment
@Component(dependencies = ContainerActivityComponent.class, modules = FragmentModule.class)
public interface SplashComponent {
    void inject(SplashFragmentView fragment);
}
