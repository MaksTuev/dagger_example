package ru.surfstudio.daggerexample.ui.screen.splash;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import ru.surfstudio.daggerexample.R;
import ru.surfstudio.daggerexample.ui.base.activity.BaseActivity;

public class SplashActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (savedInstanceState == null) {
            addFragment(R.id.container, new SplashFragmentView(), "AAA");
        }
    }

    protected void addFragment(int containerViewId, Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = this.getFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment, tag);
        fragmentTransaction.commit();
    }
}
