package ru.surfstudio.daggerexample.ui.screen.splash;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.interactor.bookfull.BookFullInteractor;
import ru.surfstudio.daggerexample.ui.base.BasePresenter;
import ru.surfstudio.daggerexample.ui.base.dialog.DialogManager;
import ru.surfstudio.daggerexample.ui.base.fragment.PerFragment;

/**
 * presenter экрана сплеша
 */
@PerFragment
public class SplashPresenter extends BasePresenter<SplashFragmentView> {

    private DialogManager dialogManager;
    private BookFullInteractor bookFullInteractor;

    @Inject
    public SplashPresenter(DialogManager dialogManager, BookFullInteractor bookFullInteractor) {
        this.dialogManager = dialogManager;
        this.bookFullInteractor = bookFullInteractor;
    }

    @Override
    public void onLoad() {
        super.onLoad();
        //smth
    }
}
