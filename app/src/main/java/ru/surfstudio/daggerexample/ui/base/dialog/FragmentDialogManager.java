package ru.surfstudio.daggerexample.ui.base.dialog;

import android.app.Fragment;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.ui.base.fragment.PerFragment;

/**
 * отвечает за показывание и скрывание диалогов из презентера, когда вью является Fragment
 */
@PerFragment
public class FragmentDialogManager implements DialogManager {

    private Fragment fragment;

    @Inject
    public FragmentDialogManager(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void show(BaseDialog dialog) {
        dialog.show(fragment);
    }
}
