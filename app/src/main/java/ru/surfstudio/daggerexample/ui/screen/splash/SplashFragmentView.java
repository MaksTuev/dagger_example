package ru.surfstudio.daggerexample.ui.screen.splash;

import javax.inject.Inject;

import ru.surfstudio.daggerexample.ui.base.BasePresenter;
import ru.surfstudio.daggerexample.ui.base.fragment.BaseFragmentView;
import ru.surfstudio.daggerexample.ui.base.fragment.FragmentModule;


/**
 * view экрана сплеша
 */
public class SplashFragmentView extends BaseFragmentView {

    @Inject
    SplashPresenter presenter;

    @Override
    protected void satisfyDependencies() {
        DaggerSplashComponent.builder()
                .containerActivityComponent(getBaseActivity().getContainerActivityComponent())
                .fragmentModule(new FragmentModule(this))
                .build()
                .inject(this);
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public String getName() {
        return "Splash";
    }
}
