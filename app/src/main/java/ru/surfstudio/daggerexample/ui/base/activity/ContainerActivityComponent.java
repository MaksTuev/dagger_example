package ru.surfstudio.daggerexample.ui.base.activity;

import android.content.Context;

import dagger.Component;
import ru.surfstudio.daggerexample.app.AppComponent;
import ru.surfstudio.daggerexample.interactor.bookfull.BookFullInteractor;
import ru.surfstudio.daggerexample.module.SessionStorage;
import ru.surfstudio.daggerexample.module.player.BookAudioPlayer;
import ru.surfstudio.daggerexample.module.player.PlayerBookStateProvider;
import ru.surfstudio.daggerexample.module.player.core.AudioPlayerCore;
import ru.surfstudio.daggerexample.module.player.storage.StaticPlayerBookStateRepository;
import ru.surfstudio.daggerexample.module.repository.BookRepository;
import ru.surfstudio.daggerexample.ui.base.fragment.BaseFragmentView;


/**
 * компонет activity, используемый в качестве {@link Component#dependencies()} в компонентах экранов,
 * у которых view наследуется от {@link BaseFragmentView}
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ContainerActivityComponent {

    //пробрасываем объекты из скоупа PerActivity в скоуп PerFragment
    Context context();
    SessionStorage sessionStorage();

    BookAudioPlayer bookAudioPlayer();
    PlayerBookStateProvider playerBookStateProvider();
    AudioPlayerCore audioPlayerCore();
    StaticPlayerBookStateRepository staticPlayerBookStateRepository();

    BookRepository bookRepository();

    BookFullInteractor bookFullInteractor();

}
