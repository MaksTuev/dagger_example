package ru.surfstudio.daggerexample.ui.base;

public interface HasPresenter {
    BasePresenter getPresenter();
    void initPresenter();
}
