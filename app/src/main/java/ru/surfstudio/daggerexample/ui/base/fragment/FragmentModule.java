package ru.surfstudio.daggerexample.ui.base.fragment;

import android.app.Fragment;

import dagger.Module;
import dagger.Provides;
import ru.surfstudio.daggerexample.ui.base.dialog.DialogManager;
import ru.surfstudio.daggerexample.ui.base.dialog.FragmentDialogManager;

@Module
public class FragmentModule {
    private Fragment fragment;

    public FragmentModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    Fragment provideFragment(){
        return fragment;
    }

    @Provides
    @PerFragment
    DialogManager provideDialogManager(FragmentDialogManager dialogManager){
        return dialogManager;
    }
}
