package ru.surfstudio.daggerexample.ui.screen.main;

import dagger.Component;
import ru.surfstudio.daggerexample.app.AppComponent;
import ru.surfstudio.daggerexample.ui.base.activity.ActivityModule;
import ru.surfstudio.daggerexample.ui.base.activity.PerActivity;

@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
@PerActivity
public interface MainComponent {
    void inject(MainActivityView obj);
}
